variable "ec2_props" {
  type = object({
    instance_name = string
    sg_name = string
    vpc_id = string
    subnet_id = string
    ami = string
    instance_type = string
    public_key_name = string
    ssh_pubkey = string
    ebs_volume_size = string
    ebs_volume_type = string
    ebs_kms_key_id = string
    ebs_iops = string
    user_data_script_name = string
  })
}

variable "ts_auth_key" {
  type = string
}

locals {
  common_tags = {
  }
}
